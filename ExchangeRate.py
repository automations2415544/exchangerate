from skpy import Skype, SkypeChats, SkypeAuthException
from dotenv import load_dotenv
import requests
import json
import os
import time
import logging
import datetime
from logging.handlers import RotatingFileHandler
import random

# Load environment variables from the .env file
load_dotenv()

skype_id = os.getenv("SKYPE_ID")
skype_email = os.getenv("SKYPE_EMAIL")
skype_pwd = os.getenv("SKYPE_PASSWORD")
token_file = os.getenv("SKYPE_TOKEN_PATH")
log_file = os.getenv("LOG_PATH")

# Constants for retry behavior
DEFAULT_RETRY_DELAY = 3600  # Initial retry delay (60 minutes)
MAX_RETRIES = 1

# Set up logging with rotation
handler = RotatingFileHandler(log_file, maxBytes=104857600, backupCount=3)  # 100MB per file, 3 backups
logging.basicConfig(
    handlers=[handler],
    level=logging.INFO,
    format="%(asctime)s - %(levelname)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S"
)

def exponential_backoff(retries):
    """ Implements exponential backoff with jitter """
    base_wait = DEFAULT_RETRY_DELAY  # Start with 15 minutes
    max_wait = 3600  # Cap at 1 hour (3600 seconds)
    
    if retries == 1:
        wait_time = base_wait + random.randint(0, 10)
    else:
        # Calculate exponential backoff with jitter
        wait_time = min(base_wait * (2 ** (retries - 1)) + random.randint(0, 10), max_wait)

    logging.info(f"Waiting {wait_time} seconds (~{wait_time // 60} minutes) before retrying...")
    time.sleep(wait_time)

def get_json_content(url, headers):
    """ Fetch JSON content from a given URL """
    try:
        response = requests.get(url, headers=headers, timeout=10)
        response.raise_for_status()
        return response.json()
    except requests.RequestException as e:
        logging.error(f"Error fetching JSON data: {e}")
        return None

def authenticate_skype(skype_email, skype_pwd, token_file, use_token=True, max_retries=MAX_RETRIES):
    """
    Authenticate with Skype using a token file or credentials.
    """
    retries = 1
    while retries <= max_retries:
        try:
            if use_token and os.path.exists(token_file):
                logging.info("Attempting to authenticate using the token file...")
                sk = Skype(tokenFile=token_file)

                # Get token expiration time
                token_expiry = sk.conn.tokenExpiry.get("skype")
                if token_expiry and token_expiry > datetime.datetime.now():
                    logging.info(f"Token is still valid until {token_expiry}. No need to re-authenticate.")
                    return sk
                
                logging.warning("Token has expired. Re-authenticating...") 
                raise SkypeAuthException("Token has expired.")

            
            logging.info(f"Authentication attempt {retries}/{MAX_RETRIES} using username and password.")

            if not skype_email or not skype_pwd:
                logging.error("No username or password provided, and no valid token file.")
                return None

            exponential_backoff(retries)
            sk = Skype(skype_email, skype_pwd, tokenFile=token_file)

            logging.info("Skype authentication successful.")
            return sk

        except SkypeAuthException as e:
            logging.error(f"Authentication failed: {e}")

            if "Auth rate limit exceeded" in str(e):
                logging.warning("Auth rate limit exceeded. Increasing retry delay.")
                retries += 1
            
            if "expired" in str(e) or "no valid token file" in str(e) or "Token has expired" in str(e):
                logging.warning("Token expired or missing. Switching to password authentication.")
                use_token = False  # Force re-authentication

            if retries > max_retries:
                logging.error("Max retries reached. Authentication failed.")
                return None

    return None

def send_message_to_skype(sk, text, skype_id):
    """
    Sends a message via Skype if it's a weekday.
    """
    if not sk:
        logging.error("No valid Skype session. Message not sent.")
        return

    current_day = datetime.datetime.today().weekday()

    if current_day not in [5, 6]:  # Monday to Friday
        try:
            ch = sk.chats[skype_id]
            ch.sendMsg(text)
            logging.info("Message sent successfully.")
        except Exception as e:
            logging.error(f"Failed to send message: {e}")
    else:
        logging.info("Skipping message send. It's the weekend.")

def get_skype_chat_id(skype_email, skype_pwd):
    """
    Fetch recent Skype chat IDs.
    """
    try:
        sk = Skype(skype_email, skype_pwd)
        print(sk.recent())
    except SkypeAuthException as e:
        logging.error(f"Failed to fetch Skype chat IDs: {e}")

def assemble_exchange_rate_info(json_data):
    """
    Assemble exchange rate info into a message.
    """
    if not json_data:
        return "Error: Unable to fetch exchange rate data."

    effectivity_date = json_data.get('effectivityDate', 'Unknown Date')
    data_list = json_data.get('dataList', [])
    filtered_data = get_exchange_rate_data(data_list)

    usd_data = filtered_data.get('USD')
    eur_data = filtered_data.get('EUR')

    text = f"Today's Exchange Rate (Last Updated: {effectivity_date}) : \n"

    if usd_data:
        text += f"{usd_data['currencyName']} = PHP {usd_data['buyingRate']} \n"
    
    if eur_data:
        text += f"{eur_data['currencyName']} = PHP {eur_data['buyingRate']}"
    
    return text

def get_exchange_rate_data(data_list):
    """
    Extract relevant exchange rate data.
    """
    currencies = ['USD', 'EUR']
    return {
        item["currencyCode"]: {
            "currencyCode": item["currencyCode"],
            "currencyName": item["currencyName"],
            "sellingRate": item["sellingRate"],
            "buyingRate": item["buyingRate"]
        }
        for item in data_list if item["currencyCode"] in currencies
    }

# BDO Forex URL
url = 'https://www.bdo.com.ph/content/bdounibank/en-ph/forex.forexListing.json'
headers = {
    'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 '
                  '(KHTML, like Gecko) Chrome/99.0.4844.84 Safari/537.36',
    'Accept': 'application/json',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.9',
}

# Fetch and process exchange rates
json_data = get_json_content(url, headers)
text = assemble_exchange_rate_info(json_data)
logging.info(text)

# Authenticate and send message
if "Error: Unable to fetch exchange rate data." not in text:
    sk = authenticate_skype(skype_email, skype_pwd, token_file, use_token=True)
    send_message_to_skype(sk, text, skype_id)
